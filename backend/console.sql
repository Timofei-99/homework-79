drop database if exists homework79;
create database if not exists homework79;

use homework79;

create table if not exists categories
(
    id          int          not null auto_increment primary key,
    title       varchar(255) not null,
    description text         null
);

create table if not exists location
(
    id           int          not null auto_increment primary key,
    name         varchar(255) not null,
    description text         null
);

create table if not exists subject
(
    id           int          not null auto_increment primary key,
    category_id  int          not null,
    location_id  int          not null,
    name varchar(255) not null,
    description  text         null,
    image        varchar(255) null,
    date datetime default CURRENT_TIMESTAMP not null ,
    constraint subject_category_id_fk
        foreign key (category_id)
            references categories (id)
            on update cascade
            on delete restrict,
    constraint subject_location_id_fk
        foreign key (location_id)
            references location (id)
            on update cascade
            on delete restrict
);

insert into categories (title, description)
values ('Computer equipment', 'Software'),
       ('Appliances', 'Goods for a kitchen'),
       ('Furniture', 'Household products');

insert into location (name, description)
values ('Director office', 'Place for director'),
       ('Office 204', 'Place for personal');

insert into subject (category_id, location_id, name, description)
values (1, 2, 'Something', 'Description');