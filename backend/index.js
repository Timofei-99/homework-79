const express = require('express');
const cors = require('cors');
const categories = require('./app/categories');
const location = require('./app/location');
const subject = require('./app/subject');
const mysqlDb = require('./mysqlDb');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;
app.use('/categories', categories);
app.use('/location', location);
app.use('/subject', subject);

mysqlDb.connect().catch(e => console.log(e));

app.listen(port, () => {
    console.log(`We are on port ${port}`);
});