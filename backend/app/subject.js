const express = require('express');
const mysqlDb = require('../mysqlDb');
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require('path');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename(req, file, cb) {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const [subject] = await mysqlDb.getConnection().query(
            'SELECT  id, name, category_id, location_id FROM ??',
            ['subject']
        );
        res.send(subject);
    } catch (e) {
        res.send({error: e});
    }
});

router.get('/:id', async (req, res) => {
    try {
        const [subjectById] = await mysqlDb.getConnection().query(
            'SELECT * FROM subject where id = ?',
            [req.params.id]
        );

        if (subjectById[0] === undefined) {
            res.send('Not found');
        }
        res.send(subjectById);
    } catch (e) {
        res.send({error: e});
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    if (req.file) {
        req.body.image = req.file.filename;
    }

    if (req.body.name && req.body.category_id && req.body.location_id) {
        try {
            const newSubject = await mysqlDb.getConnection().query(
                'INSERT INTO subject (category_id, location_id, name, description, image) values  (?, ?, ?, ?, ?)',
                [
                    req.body.category_id,
                    req.body.location_id,
                    req.body.name,
                    req.body.description,
                    req.body.image]
            );
            res.send({id: newSubject[0].insertId, ...req.body})
        } catch (e) {
            console.log(e);
            return res.status(500).send(e);
        }

    } else {
        res.status(400).send({error: 'Not correct data'});
    }
});


module.exports = router;