const express = require('express');
const mysqlDb = require('../mysqlDb');


const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const [location] = await mysqlDb.getConnection().query(
            'SELECT id, name FROM ??',
            ['location'],
        );
        res.send(location);
    } catch (e) {
        res.status(400).send({error: e});
    }
});

router.get('/:id', async (req, res) => {
    try {
        const [locationById] = await mysqlDb.getConnection().query(
            'SELECT * FROM ?? where id = ?',
            ['location', req.params.id]
        );
        res.send(locationById);
    } catch (e) {
        res.status(400).send({error: e});
    }
});

router.post('/', async (req, res) => {
    if (req.body.name) {
        const location = {
            name: req.body.name,
            description: req.body.description
        };
        const newLocation = await mysqlDb.getConnection().query(
            'INSERT into ?? (name, description) values (?, ?)',
            ['location', location.name, location.description]
        );
        res.send({id: newLocation[0].insertId, ...location,});
    } else {
        res.status(404).send({error: 'Data not valid'});
    }

});

router.delete('/:id', async (req, res) => {
    try {
        await mysqlDb.getConnection().query(
            'DELETE FROM ?? where id = ?',
            ['location', req.params.id]
        );
        res.send({message: `Deleted id=${req.params.id}`});
    } catch (e) {
        res.status(400).send({error: e});
    }
})


module.exports = router;