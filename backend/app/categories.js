const express = require('express');
const mysqlDb = require('../mysqlDb');


const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const [categories] = await mysqlDb.getConnection().query(
            'SELECT id, title FROM ??',
            ['categories']);
        res.send(categories);
    } catch (e) {
        res.status(404).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const [categories] = await mysqlDb.getConnection().query(
            'SELECT * FROM ?? where id = ?',
            ['categories', req.params.id]
        );
        res.send(categories);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/', async (req, res) => {
    if (req.body.title) {
        const category = {
            title: req.body.title,
            description: req.body.description
        };
        const categories = await mysqlDb.getConnection().query(
            'INSERT INTO ?? (title, description) values (?, ?)',
            ['categories', category.title, category.description]
        );
        res.send({...category, id: categories[0].insertId});
    } else {
        res.status(400).send({error: 'Data not valid'});
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await mysqlDb.getConnection().query(
            'DELETE FROM ?? where id=?',
            ['categories', req.params.id],
        );
        res.send({message: `Deleted item with id=${req.params.id}`});
    } catch (e) {
        res.status(400).send({error: e});
    }
});


module.exports = router;